import { Router3Page } from './app.po';

describe('router3 App', function() {
  let page: Router3Page;

  beforeEach(() => {
    page = new Router3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
