# Router3

This is just the very basics of how to set up angular 2, router 3.  There is, of course, heaps more
functionality available.

http://angularjs.blogspot.co.uk/2016/06/improvements-coming-for-routing-in.html

https://medium.com/@blacksonic86/upgrading-to-the-new-angular-2-router-255605d9da26

Assumption: the components that represent the route targets have already created.

The components in this project that are implement the routes are in the following folders:-
src/app/home
src/app/list
src/app/list/list-detail

Unless they do any further routing themselves, then no changes are needed to these components.

This example use angular 2 router 3. We need "@angular/router": "3.0.0-beta.2" in our package.json.

1. Create a route configuration file at app level (or wherever the root of our routing is).
   The route configuration file in this project is at src/app/app-routes-config.ts.

2. Add the file created in 1. to our bootstrap (src/main.ts) to make the routes global and singleton.
   We can now inject the router anywhere we wish to refer to it.

3. In our AppComponent class in src/app/app.component.ts we need the ROUTER_DIRECTIVES from @angular/router.

4. In our src/app/app-component.html we create the link tags for our route targets using the routerLink directive.

See src/list/ for how to set route parameters.
See src/list/list-detail/ for how to read and subscribe to route parameters.

