import { bootstrap } from '@angular/platform-browser-dynamic';
import { provideRouter } from '@angular/router';
import { enableProdMode } from '@angular/core';
import { AppComponent, environment } from './app/';
import { AppRoutesConfig } from './app/app-routes-config';

if (environment.production) {
  enableProdMode();
}

// Make our routes collection global and singleton.
bootstrap(AppComponent,
          provideRouter(AppRoutesConfig));
