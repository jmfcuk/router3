import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'test-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  // We need this so we can use routerLink and the <router-outlaet> tag in the html.
  directives: [ROUTER_DIRECTIVES] 
})
export class AppComponent {

  currentUrl: string;

  // Inject the router, we want to keep tabs on the url so we can display it.
  constructor(private router: Router) {

    this.router.events.subscribe(e => this.currentUrl = e.url);
  }
}


