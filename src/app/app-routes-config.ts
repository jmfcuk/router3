// The components we are routing to.
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import { ListDetailComponent } from './list/list-detail/list-detail.component';
import { SomeplaceElseComponent } from './someplace-else/someplace-else.component';

// Define which component each route points to.
export const AppRoutesConfig = [
  { path: '', component: HomeComponent }, // empty = default route.
  { path: 'list', component: ListComponent },
  { path: 'list/:id', component: ListDetailComponent }, // Here we define a param called id.
];


