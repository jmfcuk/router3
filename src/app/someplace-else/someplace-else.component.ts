import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-someplace-else',
  templateUrl: 'someplace-else.component.html',
  styleUrls: ['someplace-else.component.css']
})
export class SomeplaceElseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
