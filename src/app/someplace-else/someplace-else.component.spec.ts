/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { SomeplaceElseComponent } from './someplace-else.component';

describe('Component: SomeplaceElse', () => {
  it('should create an instance', () => {
    let component = new SomeplaceElseComponent();
    expect(component).toBeTruthy();
  });
});
