import { Component, OnInit } from '@angular/core';
// We will need this to get at the params.
import { ActivatedRoute } from '@angular/router';
import { Item } from './item';
import { LIST_DETAIL } from './mock/list-detail-data'; 

@Component({
  moduleId: module.id,
  selector: 'app-list-detail',
  templateUrl: 'list-detail.component.html',
  styleUrls: ['list-detail.component.css']
})
export class ListDetailComponent implements OnInit {

  private _items: Array<Item>;

  item: Item;

  // We want the route so we can get at the params.
  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {

    this._items = LIST_DETAIL;

    // Grab the id parameter that we defined in src/app/app-routes.ts.
    let id = this.route.snapshot.params['id'];
    
    this.item = this.getItem(id);

    // We can subscribe to params if we like
    /*
    this.route.params.subscribe(p => 
      {
        console.log('id = ' + p['id']);
        this.item = this.getItem(+ p['id']);
      });
    */
  }

  getItem(id: number): Item {

    let itm: Item;

    for (let item of this._items) {

      if (id == item.id) {
        itm = item;
        break;
      }
    }

    return itm;
  }
}




