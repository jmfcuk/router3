import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, RouterLink } from '@angular/router';
import { Item } from './list-detail/item'; 
import { LIST_DETAIL } from './list-detail/mock/list-detail-data'; 

@Component({
  moduleId: module.id,
  selector: 'app-list',
  templateUrl: 'list.component.html',
  styleUrls: ['list.component.css'],
  directives: [ROUTER_DIRECTIVES] // We need this so we can use routerLink in the html.
})
export class ListComponent implements OnInit {

  items: Array<Item>;

  constructor() { }

  ngOnInit() {

    this.items = LIST_DETAIL;
  }
}

